# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"
Vagrant.require_version ">= 1.5"

# Project sources root folder
project_root = "/vagrant/docler"
www_root = "/vagrant/docler/code"
host_name = "doclertest"
ip_address = "192.168.33.99"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.provider :virtualbox do |v|
      v.name = "doclertest"
      v.customize [
          "modifyvm", :id,
          "--memory", 2048,
          "--cpus", 2,
      ]
  end

  config.vm.box = "ubuntu/trusty64"
  config.vm.hostname = host_name

  config.vm.network "forwarded_port", guest: 80, host: 8000
  config.vm.network "private_network", ip: ip_address
  # config.vm.network "public_network"
  config.ssh.forward_agent = true
  config.vm.synced_folder project_root, project_root, create:true,
  :owner => "vagrant",
  :group => "www-data",
  :mount_options => ["dmode=775","fmode=664"]

  config.ssh.pty = true

  #############################################################
  # Ansible provisioning (you need to have ansible installed)
  #############################################################

  # Ansible "exec 8" trouble resolving command:
  config.vm.provision "shell", inline: "find /vagrant/docler/.vagrant -type f -exec chmod -x {} \\;"
  config.vm.provision "ansible" do |ansible|
      ansible.playbook = "ansible/playbook.yml"
      ansible.limit = 'all'
      ansible.extra_vars = {
          private_interface: ip_address,
          hostname: host_name
      }
  end
  config.vm.provision "shell", path: "./provisioner", args: [project_root, www_root]
end