#  Test task

## Requirements

- [Ansible provisioner](http://docs.ansible.com/) Hint: On OSX use homebrew: ```brew install ansible```
- [Vagrant](https://www.vagrantup.com/)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

## Instructions

* Add `192.168.33.99 docler.dev` to `/etc/hosts`
* `git clone <this repository url> /vagrant/docler`
* `cd /vagrant/docler`
* `vagrant up --provision`
* copy `code/.env.example` to `code/.env` and change mailtrap.io settings for emails receiving. 

If shell provisioner will process too long, you can run it manually on host machine: 

`./provisioner "/vagrant/docler" "/vagrant/docler/code"`

### Captcha test

* Open http://docler.dev/auth/login
* Insert incorrect data
* Press Submit many times. When some counter will reach the limit, captcha image becomes more complicated

### Password test

* Open http://docler.dev/auth/register
* Enter soft password and press "Register"

### Email test

* Sucesfully register a new user
* Wait for 3 minutes
* Refresh page (access will be denied)
* Go to mailtrap.io and click on link


