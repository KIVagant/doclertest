<?php

namespace App\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Cache;

trait AdvancedThrottlesLogins
{
    protected $last_auth_login_key = 'last_auth_login';
    /**
     * Get the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function getLoginAttemptsByUsername(Request $request)
    {
        // TODO: Here should be a multiply getting
        $key = $this->getLoginAttemptsKeyByUsername($request);
        $cache = $key ? Cache::get($key) : 0;

        return (int) $cache;
    }

    /**
     * Get the login attempts for the IP.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function getLoginAttemptsByIP(Request $request)
    {
        // TODO: Here should be a multiply getting
        $key = $this->getLoginAttemptsKeyByIP($request);
        $cache = $key ? Cache::get($key) : 0;

        return (int) $cache;
    }

    /**
     * Get the login attempts for the IP Range.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function getLoginAttemptsByIPRange(Request $request)
    {
        // TODO: Here should be a multiply getting
        $key = $this->getLoginAttemptsKeyByIPRange($request);
        $cache = $key ? Cache::get($key) : 0;

        return (int) $cache;
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function incrementLoginAttemptsByUsername(Request $request)
    {
        $expiresAt = Carbon::now()->addSeconds(3600);
        Cache::add($key = $this->getLoginAttemptsKeyByUsername($request), 0, $expiresAt);

        return (int) Cache::increment($key);
    }

    /**
     * Increment the login attempts for the IP.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function incrementLoginAttemptsByIP(Request $request)
    {
        $expiresAt = Carbon::now()->addSeconds(3600);
        Cache::add($key = $this->getLoginAttemptsKeyByIP($request), 0, $expiresAt);

        return (int) Cache::increment($key);
    }

    /**
     * Increment the login attempts for the IP Range.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function incrementLoginAttemptsByIPRange(Request $request)
    {
        $expiresAt = Carbon::now()->addSeconds(3600);
        Cache::add($key = $this->getLoginAttemptsKeyByIPRange($request), 0, $expiresAt);

        return (int) Cache::increment($key);
    }

    protected function getLoginUsernameFromLastAuth()
    {
        return session($this->last_auth_login_key);
    }

    protected function setLoginUsernameFromLastAuth($name)
    {
        session([$this->last_auth_login_key => $name]);
    }
    /**
     * Get the login attempts cache key.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getLoginAttemptsKeyByUsername(Request $request)
    {
        $username = is_callable($this, 'loginUsername') && $request->isMethod('post')
            ? $request->input($this->loginUsername())
            : $this->getLoginUsernameFromLastAuth();

        return $username
            ? 'login:attempts:name:'.md5($username)
            : null;
    }

    /**
     * Get the login attempts cache key by full IP.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getLoginAttemptsKeyByIP(Request $request)
    {
        $ip = $request->ip();

        return $ip
            ? 'login:attempts:ip:'.md5($request->ip())
            : null;
    }

    /**
     * Get the login attempts cache key by part of IP.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getLoginAttemptsKeyByIPRange(Request $request)
    {
        $ip = $request->ip();
        if ($ip) {
            if (strstr($ip, ':')) { // ipv6
                $ip_ar = explode(':', $ip);
                $protocol = 6;
            } else {
                $ip_ar = explode('.', $ip);
                $protocol = 4;
            }
            array_pop($ip_ar);
            if (6 === $protocol) {
                $ip = implode(':', $ip_ar);
            } else {
                $ip = implode('.', $ip_ar);
            }

            return 'login:attempts:range:' . md5($ip);
        }

        return null;
    }

    protected function clearLoginAttemptsKeyByUsername(Request $request)
    {
        \Cache::forget($this->getLoginAttemptsKeyByUsername($request));
    }

    protected function clearLoginAttemptsKeyByIP(Request $request)
    {
        \Cache::forget($this->getLoginAttemptsKeyByIP($request));
    }

    protected function clearAllLoginAttemptsCounters(Request $request)
    {
        \Cache::forget($this->getLoginAttemptsKeyByUsername($request));
        \Cache::forget($this->getLoginAttemptsKeyByIP($request));
        \Cache::forget($this->getLoginAttemptsKeyByIPRange($request));
    }
}