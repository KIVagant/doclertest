<?php

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('activate/{code}', 'Auth\AuthController@activateAccount');

Route::any('greeting', 'GreetingController@index');

Route::get('/', function () {
    return view('welcome');
});
