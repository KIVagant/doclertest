<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->auth->user();
        if (\Config::get('auth.email_confirm.enabled') && $user) {
            $now = Carbon::create();
            $diff = $user->created_at->diffInMinutes($now);
            if (!$user->active && $diff > \Config::get('auth.email_confirm.lock_access_after_days')) {
                $this->auth->logout();
                \Flash::info('Please, confirm your email address to proceed. Check your inbox and press confirmation link.');

                return redirect()->guest('/');
            }
        }
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        }

        return $next($request);
    }
}
