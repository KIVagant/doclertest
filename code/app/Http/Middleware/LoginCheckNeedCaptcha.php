<?php

namespace App\Http\Middleware;

use Closure;
use App\Auth\AdvancedThrottlesLogins;

class LoginCheckNeedCaptcha
{
    use AdvancedThrottlesLogins;

    public function loginUsername()
    {
        return null; // no request
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $counters = [
            'key_by_name' => $this->getLoginAttemptsKeyByUsername($request),
            'key_by_ip' => $this->getLoginAttemptsKeyByIP($request),
            'key_by_range' => $this->getLoginAttemptsKeyByIPRange($request),
            'by_name' => $this->getLoginAttemptsByUsername($request),
            'by_ip' => $this->getLoginAttemptsByIP($request),
            'by_range' => $this->getLoginAttemptsByIPRange($request),
        ];

        $limits = \Config::get('captcha.limits.login');
        view()->share('limits', $limits);
        view()->share('counters', $counters);

        if ($this->getLoginAttemptsByIPRange($request) >= $limits['by_ip_range_strong']) {
            session(['captcha_check' => 'inverse_strong']);
        } else if ($this->getLoginAttemptsByIPRange($request) >= $limits['by_ip_range']) {
            session(['captcha_check' => 'inverse']);
        } else if ($this->getLoginAttemptsByUsername($request) >= $limits['by_username']
            || $this->getLoginAttemptsByIP($request) >= $limits['by_ip']) {
            session(['captcha_check' => 'inverse_mini']);
        } else {
            session(['captcha_check' => '']);
        }
        return $next($request);
    }
}
