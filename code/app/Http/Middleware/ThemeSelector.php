<?php

namespace App\Http\Middleware;

use Closure;

class ThemeSelector
{
    const DEFAULT_THEME = 'superhero';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $theme = $request->session()->get('theme', static::DEFAULT_THEME);
        $theme = $request->input('theme', $theme);
        $request->session()->put('theme', $theme);
        view()->share('theme', $theme);

        return $next($request);
    }
}
