<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Auth\AdvancedThrottlesLogins;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exception\HttpResponseException;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    const THROTTLES = true;
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, AdvancedThrottlesLogins; //ThrottlesLogins
    protected $redirectPath = '/greeting';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', [
            'except' => ['getLogout', 'activateAccount']
        ]);
        $this->middleware('login_captcha', [
            'except' => ['getLogout', 'activateAccount']
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6|numbers|letters|symbols',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Handle a login request to the application.
     *
     * @see \Illuminate\Foundation\Auth\AuthenticatesUsers@postLogin (overrided)
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        if ($request->input('clear_counters')) {
            $this->clearAllLoginAttemptsCounters($request);
        }
        $this->validateLoginRequest($request);

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, static::THROTTLES);
        }

        $this->setLoginUsernameFromLastAuth($request->input($this->loginUsername()));

        $this->incrementAttemptsCounters($request);

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * @param Request $request
     */
    protected function validateLoginRequest(Request $request)
    {
        $login_key = $this->loginUsername();
        $rules = [
            $login_key => 'required',
            'password' => 'required',
        ];
        if (static::THROTTLES && session('captcha_check')) {
            $rules['captcha'] = 'required|captcha';
        }
        $messages = [
            'captcha' => 'Please, retype the all symbols that appear on captcha image',
            'password' => 'Invalid credentials',
            $login_key => 'Invalid ' . $login_key,
        ];
        try {
            $this->validate($request, $rules, $messages);
        } catch (HttpResponseException $e) {
            $this->incrementAttemptsCounters($request);
            throw $e;
        }
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttemptsKeyByUsername($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * @param Request $request
     */
    protected function incrementAttemptsCounters(Request $request)
    {
        if (static::THROTTLES) {
            $this->incrementLoginAttemptsByUsername($request);
            $this->incrementLoginAttemptsByIP($request);
        }
        // TODO: Here must be !$request->session, but for more comfortable testing this condition was flipped
        // Correct specification: "however the ranges are only increased when there is no captcha displayed from any reason."
        if (static::THROTTLES && $request->session()->get('captcha')) {
            $this->incrementLoginAttemptsByIPRange($request);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());
        Auth::login($user);

        $this->sendConfirmationLetter($user, true);
        \Flash::info('Welcome! We are send confirmation letter to your email address. You have 3 days to activate your account. (Actually only 3 minutes, but who cares?)');

        return redirect($this->redirectPath());
    }

    /**
     * @param Request $request
     * @param $user
     */
    protected function sendConfirmationLetter(User $user, $renew = false)
    {
        $activation_code = str_random(60) . '--' . $user->getAttribute('email');
        $data = array(
            'name' => $user->getAttribute('name'),
            'email' => $user->getAttribute('email'),
            'code' => $activation_code,
        );
        if ($renew) {
            $user->activation_code = $activation_code;
            $user->save();
        }

        \Mail::queue('emails.activate_account', $data, function ($message) use ($data) {
            $message->subject('Please, confirm your email address');
            $message->to($data['email']);
        });
    }

    /**
     * @param $code
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function activateAccount($code, User $user, Request $request)
    {
        try {
            $user->accountIsActive($code);
            \Flash::success('Your email has been confirmed!');

            return redirect()->intended($this->redirectPath());
        } catch (ModelNotFoundException $e) {
            $ar = explode('--', $code);
            if (count($ar) === 2) {
                $email = array_pop($ar);
                $user = User::where('email', '=', $email)->firstOrFail();

                $this->sendConfirmationLetter($user, true);
                \Flash::warning('Your email could not be confirmed. Please, check your inbox again for a new confirmation letter.');
            } else {
                \Flash::error('Your email could not be confirmed. Try again.');
            }

            return redirect('/');

        }
    }
}
