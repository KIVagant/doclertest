<ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Themes <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="?theme=lumen">lumen</a></li>
            <li><a href="?theme=cerulean">cerulean</a></li>
            <li><a href="?theme=cosmo">cosmo</a></li>
            <li><a href="?theme=flatly">flatly</a></li>
            <li><a href="?theme=united">united</a></li>
            <li class="divider">Darks</li>
            <li><a href="?theme=cyborg">cyborg</a></li>
            <li><a href="?theme=darkly">darkly</a></li>
            <li><a href="?theme=slate">slate</a></li>
            <li><a href="?theme=superhero">superhero</a></li>
        </ul>
    </li>
</ul>