<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Welcome, {{$name}}!</h2>
<div>
    Click to link to confirm your email address:
    <a href="{{ url('activate/' . $code) }}" >
        {{ url('activate/') }}
    </a>
</div>
</body>
</html>

