@extends('common')

@section('content')

    <h1>Registration</h1>
    {!! Form::open(['url' => '/auth/register']) !!}
    {!! csrf_field() !!}

    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'size' => 100] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', old('email'), ['class' => 'form-control', 'size' => 100] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password', 'Password:') !!}
        {!! Form::password('password', ['class' => 'form-control', 'size' => 100] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password_confirmation', 'Confirm Password:') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'size' => 100] ) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Register', ['class' => 'btn btn-success'] ) !!}
    </div>
    {!! Form::close() !!}
@endsection