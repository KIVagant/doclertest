@extends('common')

@section('content')

    <h1>Login</h1>
    {!! Form::open(['url' => '/auth/login']) !!}
    {!! csrf_field() !!}

    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', old('email'), ['class' => 'form-control', 'size' => 100] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password', 'Password:') !!}
        {!! Form::password('password', ['class' => 'form-control', 'size' => 100] ) !!}
    </div>

    @if(session('captcha_check'))
    <div class="form-group">
        {!! Form::label('captcha', 'Please, type the symbols that appear on the right into the entry box.') !!}
        <div class="row">
            <div class="col-md-4" >
                {!! Form::text('captcha', old('captcha'), ['class' => 'form-control', 'size' => 6] ) !!}
            </div>
            <div class="col-md-2" >
                {!! captcha_img(session('captcha_check')) !!}
            </div>
        </div>
    </div>
    @endif
    <div class="form-group">
        {!! Form::checkbox('remember', 1, old('remember'), ['aria-label' => 'Remember Me'] ) !!}
        {!! Form::label('remember', 'Remember Me') !!}
    </div>
    <div class="form-group">
        {!! Form::checkbox('clear_counters', 1, false, ['aria-label' => 'Clear all counters'] ) !!}
        {!! Form::label('remember', 'Clear all login attempts counters') !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Login', ['class' => 'btn btn-success'] ) !!}
    </div>
    {!! Form::close() !!}
@endsection