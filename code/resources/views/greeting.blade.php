@extends('common')

@section('content')

    <h1>Hello, {{Auth::user()->name}}</h1>
    <div class="title">This is a secret greeting page just for you. Enjoy and be careful with this important information!</div>
    @if(!\Auth::user()->active)
        <hr />
        <p><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Your email is not confirmed. Please, check your inbox.</p>
    @endif

@endsection