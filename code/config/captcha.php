<?php

return [
    'limits' => [
        'login' => [
            'by_username' => 3,
            'by_ip' => 5,
            'by_ip_range' => 10,
            'by_ip_range_strong' => 15,
        ]
    ],

    'default'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
    ],

    'flat'   => [
        'length'    => 6,
        'width'     => 160,
        'height'    => 46,
        'quality'   => 90,
        'lines'     => 6,
        'bgImage'   => false,
        'bgColor'   => '#ecf2f4',
        'fontColors'=> ['#2c3e50', '#c0392b', '#16a085', '#c0392b', '#8e44ad', '#303f9f', '#f57c00', '#795548'],
        'contrast'  => -5,
    ],

    'inverse_mini'   => [
        'length'    => 3,
        'width'     => 60,
        'height'    => 32,
        'invert'    => true,
        'contrast'  => 0,
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -5,
    ],
    'inverse_strong'   => [
        'length'    => 25,
        'width'     => 400,
        'height'    => 36,
        'quality'   => 75,
        'sensitive' => true,
        'angle'     => 17,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -7,
    ]
];
